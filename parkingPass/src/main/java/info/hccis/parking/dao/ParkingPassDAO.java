package info.hccis.parking.dao;

import info.hccis.parking.jpa.entity.ParkingPass;
import info.hccis.parking.util.DatabaseUtility;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ParkingPass database access object
 *
 * @author bjm
 * @since 20201001
 */
public class ParkingPassDAO {

//    public static boolean checkConnection() {
//
//        Connection conn = null;
//        try {
//            conn = DriverManager.getConnection(
//                    "jdbc:mysql://localhost:3306/cis2232_parking",
//                    "root",
//                    ""); //--No password.  For assignment submission this would be expected to be empty.
//        } catch (Exception e) {
//            return false;
//        }
//
//        return true;
//
//    }

    /**
     * Select the records from the database
     *
     * @since 20201001
     * @author BJM
     */
    public ArrayList<ParkingPass> selectAll() {
        ArrayList<ParkingPass> parkingPasses = new ArrayList();

        //Select from the database
        Connection conn = null;
//        try {
//            conn = DriverManager.getConnection(
//                    "jdbc:mysql://localhost:3306/cis2232_parking",
//                    "root",
//                    ""); //--No password.  For assignment submission this would be expected to be empty.
//        } catch (Exception e) {
//            System.out.println("Could not make a connection to the database");
//            return null;
//        }

        try {

            conn = DatabaseUtility.getConnection("");
            Statement statement = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next selectAll all the rows and display them here...
            ResultSet rs = statement.executeQuery("select * from ParkingPass");

            //Show all the parking passes
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String licensePlate = rs.getString("licensePlate");
                String program = rs.getString("program");

                ParkingPass parkingPass = new ParkingPass(id);
                
                parkingPass.setFirstName(firstName);
                parkingPass.setLastName(lastName);
                parkingPass.setLicensePlate(licensePlate);
                parkingPass.setProgram(program);

                parkingPasses.add(parkingPass);
                
            }

        } catch (SQLException ex) {
            Logger.getLogger(ParkingPassDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                conn.close();
            } catch (SQLException ex) {
                //Could not close.  
                System.out.println("Error closing the connection.");
            }
        }
        return parkingPasses;

    }

    /**
     * Insert into the database.  Note the id is expected to be 0.  If it is not 
     * 0 then the update will be used.
     *
     * @param parkingPass Parking pass object to be inserted
     * @return true if no exception occurs
     * @since 20201001
     * @author BJM
     */
    public ParkingPass insert(ParkingPass parkingPass) {

        Connection conn = null;

//        try {
//            //BJM 20200925 Removed the password.
//            conn = DriverManager.getConnection(
//                    "jdbc:mysql://localhost:3306/cis2232_parking",
//                    "root",
//                    "");
//        } catch (SQLException ex) {
//            System.out.println("SQL exception happned!");
//            return false;
//        }
//
//        if (parkingPass.getId() == 0) {

            //***************************************************
            // INSERT
            //***************************************************
            try {
                conn = DatabaseUtility.getConnection("");
                String theStatement = "INSERT INTO ParkingPass(firstName,lastName,licensePlate, program) "
                        + "VALUES (?,?,?,?)";
                PreparedStatement stmt = conn.prepareStatement(theStatement);
                stmt.setString(1, parkingPass.getFirstName());
                stmt.setString(2, parkingPass.getLastName());
                stmt.setString(3, parkingPass.getLicensePlate());
                stmt.setString(4, parkingPass.getProgram());
                stmt.executeUpdate();
//                return true;
            } catch (SQLException sqle) {
                System.out.println("sql exception caught");
                sqle.printStackTrace();
//                return false;
            }
//        } else {
            return parkingPass;
//        }

    }

    /**
     * Update into the database.
     *
     * @param parkingPass Parking pass object to be updated
     * @return true if no exception occurs
     * @since 20201001
     * @author BJM
     */
    public ParkingPass update(ParkingPass parkingPass) {

        Connection conn = null;

//        try {
//            //BJM 20200925 Removed the password.
//            conn = DriverManager.getConnection(
//                    "jdbc:mysql://localhost:3306/cis2232_parking",
//                    "root",
//                    "");
//        } catch (SQLException ex) {
//            System.out.println("SQL exception happned!");
//            return false;
//        }
//
//        if (parkingPass.getId() == 0) {
//            return false;
//        } else {

            //***************************************************
            // update
            //***************************************************
            try {
                conn = DatabaseUtility.getConnection("");
                String theStatement = "UPDATE ParkingPass set firstName=?, lastName=?, "
                        + "licensePlate=?, program=? where id=?";
                PreparedStatement stmt = conn.prepareStatement(theStatement);
                stmt.setString(1, parkingPass.getFirstName());
                stmt.setString(2, parkingPass.getLastName());
                stmt.setString(3, parkingPass.getLicensePlate());
                stmt.setString(4, parkingPass.getProgram());
                stmt.setInt(5, parkingPass.getId());
                stmt.executeUpdate();
//                return true;
            } catch (SQLException sqle) {
                System.out.println("sql exception caught");
                sqle.printStackTrace();
//                return false;
            }
            return parkingPass;
//        }
    }

    public boolean delete(int id) {
        Connection conn = null;

        //***************************************************
        // INSERT
        //***************************************************
        try {
            conn = DatabaseUtility.getConnection("");
            String theStatement = "DELETE FROM parkingpass WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setInt(1, id);

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
            return false;
        }

        return true;
    }   
}
