package info.hccis.parking.controllers;

import info.hccis.parking.bo.ParkingPassBO;
import info.hccis.parking.jpa.entity.ParkingPass;
import info.hccis.parking.repositories.ParkingRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for the parking pass functionality of the site
 *
 * @since 20200930
 * @author CIS2232 Kalyon Macphail
 */
@Controller
@RequestMapping("/parking")
public class ParkingController {

    
    private final ParkingRepository parkingRepository;

    public ParkingController(ParkingRepository cr) {
        parkingRepository = cr;
    }
    
    /**
     * Page to allow user to view parking passes
     *
     * @since 20200528
     * @author Kal (modified from Fred/Amro's project)
     */
    @RequestMapping("/list")
    public String list(Model model) {

        //Go get the parking passes from the database.
//        ParkingPassBO parkingpassBO = new ParkingPassBO();
//        ArrayList<ParkingPass> parkingpass = parkingpassBO.load();

        model.addAttribute("parkingpass", loadParkingPass());
        model.addAttribute("findNameMessage", "Parking Passes loaded: " +  ""); //add total passes

        return "parking/list";
    }

    /**
     * Page to allow user to add a parking pass
     *
     * @since 20201002
     * @author Kal (modified from Fred/Amro's project
     */
    @RequestMapping("/add")
    public String add(Model model) {

        model.addAttribute("message", "Add a parking pass");

        ParkingPass parkingpass = new ParkingPass();
        model.addAttribute("parkingpass", parkingpass);

        return "parking/add";
    }

    /**
     * Page to allow user to submit the add a parking pass. It will put the parking pass in
     * the database.
     *
     * @since 20201002
     * @author Kal (modified from Fred/Amro's project
     */
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("parkingpass") ParkingPass parkingpass, BindingResult result) {

        //If errors put the object back in model and send back to the add page.
        if (result.hasErrors()) {
            System.out.println("Validation error");
            return "parking/add";
        }

//        //Save that parking pass to the database
//        ParkingPassBO parkingpassBO = new ParkingPassBO();
//        parkingpassBO.save(parkingpass);
//
//        //reload the list
//        ArrayList<ParkingPass> parkingpasses = parkingpassBO.load();

        parkingRepository.save(parkingpass);

        String propFileName = "messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        String successAddString = rb.getString("message.parking.saved");

        model.addAttribute("message", successAddString);
        model.addAttribute("parkingpass", loadParkingPass());

        return "parking/list";
    }

    /**
     * Page to allow user to edit a parking pass
     *
     * @since 20201007
     * @author Kal (modified from Fred/Amro's project
     */
    @RequestMapping("/edit")
    public String edit(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

        //reload the list
//        ParkingPassBO parkingpassBO = new ParkingPassBO();
//        ArrayList<ParkingPass> parkingpass = parkingpassBO.load();
//
//        ParkingPass found = null;
//        for (ParkingPass current : parkingpass) {
//            if (current.getId() == id) {
//                found = current;
//                break;
//            }
//        }

        Optional<ParkingPass> found = parkingRepository.findById(id);

        model.addAttribute("parkingpass", found);
        return "parking/add";
    }

    /**
     * Page to allow user to delete a parking pass
     *
     * @since 20201009
     * @author BJM
     */
    @RequestMapping("/delete")
    public String delete(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

        String propFileName = "messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        String successString;
        try {
            parkingRepository.deleteById(id);
            successString = rb.getString("message.parking.deleted") + " (" + id + ")";
        } catch (EmptyResultDataAccessException e) {
            successString = rb.getString("message.parking.deleted.error") + " (" + id + ")";
        }
        model.addAttribute("message", successString);
        model.addAttribute("parkingpass", loadParkingPass());

        return "parking/list";
    }
    
    public ArrayList<ParkingPass> loadParkingPass() {
        ArrayList<ParkingPass> parkingpass = (ArrayList<ParkingPass>) parkingRepository.findAll();
        return parkingpass;

    }
}
