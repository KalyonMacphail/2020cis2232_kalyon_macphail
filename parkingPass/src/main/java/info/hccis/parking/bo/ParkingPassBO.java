package info.hccis.parking.bo;

import info.hccis.parking.dao.ParkingPassDAO;
import info.hccis.parking.jpa.entity.ParkingPass;
import java.util.ArrayList;
import static java.util.concurrent.locks.LockSupport.park;

/**
 * Parking pass business object
 *
 * @author CIS2232
 * @since 20201001
 */
public class ParkingPassBO {

    public static ArrayList<ParkingPass> load() {
            //Read parking passes from the database
        ParkingPassDAO parkingpassDAO = new ParkingPassDAO();
        ArrayList<ParkingPass> parkingpass = parkingpassDAO.selectAll();

        return parkingpass;
    }

    /**
     * Select all records from the database
     *
     * @return List of the parking passes
     * @since 20200923
     * @author Kal
     */
    public ArrayList<ParkingPass> selectAll() {

        //Read from the database
        ParkingPassDAO parkingPassDAO = new ParkingPassDAO();
        ArrayList<ParkingPass> parkingPasses = parkingPassDAO.selectAll();
        return parkingPasses;
    }

    /**
     * Insert the parking pass into the database
     * @since 20201001
     * @author Kal
     */
//    public boolean insert(ParkingPass parkingPass) {
//        ParkingPassDAO parkingPassDAO = new ParkingPassDAO();
//        return parkingPassDAO.insert(parkingPass);
//    }

    /**
     * Update the parking pass into the database
     * @since 20201001
     * @author Kal
     */
//    public boolean update(ParkingPass parkingPass) {
//        ParkingPassDAO parkingPassDAO = new ParkingPassDAO();
//        return parkingPassDAO.update(parkingPass);
//    }

    public boolean delete(int id) {
        ParkingPassDAO parkingpassDAO = new ParkingPassDAO();
        return parkingpassDAO.delete(id);
    }

    public ParkingPass save(ParkingPass parkingpass) {
        ParkingPassDAO parkingpassDAO = new ParkingPassDAO();

        //NOTE:  The id attribute generated from the database is an Integer not an
        //       int.  The default for an Integer is null so can't compare to 0.
        if (parkingpass.getId() == null ) {
            parkingpass = parkingpassDAO.insert(parkingpass);
        } else {
            parkingpassDAO.update(parkingpass);
        }
        
        return parkingpass;
    }


}
