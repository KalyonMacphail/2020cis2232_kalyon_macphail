package info.hccis.parking.repositories;

/**
 *
 * @author Kal
 */

import info.hccis.parking.jpa.entity.ParkingPass;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingRepository extends CrudRepository<ParkingPass, Integer> {
    
    ArrayList<ParkingPass> findAllByLastName(String lastName);
}
